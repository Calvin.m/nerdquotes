package com.rave.nerdquotes.model.remote.dto


import kotlinx.serialization.Serializable

@Serializable
data class QuoteDTO(
    val author: String,
    val en: String,
    val id: String
)