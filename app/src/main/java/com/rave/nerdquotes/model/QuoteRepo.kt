package com.rave.nerdquotes.model

import com.rave.nerdquotes.model.local.entity.Quote
import com.rave.nerdquotes.model.remote.QuoteService

/**
 * Quote repo.
 *
 * @constructor Create empty Quote repo
 */
class QuoteRepo(
    private val service: QuoteService
) {

    // old way
    // suspend fun getQuotes(count: Int): List<Quote> = withContext(Dispatchers.IO) {
    /**
     * Get quotes.
     *
     * @param count
     * @return
     */
    suspend fun getQuotes(count: Int): List<Quote> {
        val quoteDtos = service.getQuotes(count)
        return quoteDtos.map { Quote(id = it.id, author = it.author, en = it.en) }
    }
}
