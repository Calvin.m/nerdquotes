package com.rave.nerdquotes.model.remote

import com.rave.nerdquotes.model.remote.dto.QuoteDTO
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Quote service.
 *
 * @constructor Create empty Quote service
 */
interface QuoteService {
    // https://programming-quotes-api.herokuapp.com/
    // Quotes/
    // ?count=1
    @GET("quotes")
    suspend fun getQuotes(@Query("count") count: Int): List<QuoteDTO>
}
