package com.rave.nerdquotes.model.local.entity


import kotlinx.serialization.Serializable

@Serializable
data class Quote(
    val author: String,
    val en: String,
    val id: String
)
